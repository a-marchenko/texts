package com.owoxbi.texts.object.value;

import com.owoxbi.texts.dao.entity.TextEntity;
import lombok.NonNull;
import lombok.Value;

import java.io.Serializable;
import java.util.Optional;

@Value
public class Text implements Serializable {

    @NonNull
    TextNamespace namespace;

    @NonNull
    TextTranslation translation;

    public static Text from(@NonNull TextEntity textEntity, @NonNull Language language) {

        final String translation;
        switch (language) {
            case ru:
                translation = textEntity.getRu();
                break;
            case en:
                translation = textEntity.getEn();
                break;
            default:
                throw new IllegalArgumentException("Unknown language: " + language.name());
        }

        final String translationOrStub = Optional.ofNullable(translation).orElse(textEntity.getStub());

        return new Text(new TextNamespace(textEntity.getName()), new TextTranslation(translationOrStub));
    }
}

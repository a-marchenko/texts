package com.owoxbi.texts.object.value;

import com.fasterxml.jackson.annotation.JsonValue;
import lombok.NonNull;
import lombok.Value;

import java.util.regex.Pattern;

@Value
public class TextNamespace {
    @JsonValue
    @NonNull
    String namespace;

    private boolean isSub(TextNamespace textNamespace) {

        if (0 != namespace.indexOf(textNamespace.getNamespace())) {
            return false;
        }

        final String replacedString = namespace.replaceFirst(Pattern.quote(textNamespace.getNamespace()), "");
        return replacedString.isEmpty() || '.' == replacedString.charAt(0);
    }

    public boolean isSub(@NonNull String textNamespace) {
        return isSub(new TextNamespace(textNamespace));
    }

    @Override
    public String toString() {
        return namespace;
    }
}

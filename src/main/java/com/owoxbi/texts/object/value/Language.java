package com.owoxbi.texts.object.value;

public enum Language {
    ru,
    en,
    stub;

    public static Language from(String language) {
        return valueOf(language);
    }
}

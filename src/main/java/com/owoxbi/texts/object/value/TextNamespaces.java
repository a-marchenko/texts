package com.owoxbi.texts.object.value;

import lombok.NonNull;
import lombok.Value;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Value
public class TextNamespaces {
    private static final String DEFAULT_SEPARATOR = ",";

    @NonNull
    private List<TextNamespace> namespaces;

    public static TextNamespaces from(@NonNull String namespaces) {
        return new TextNamespaces(
            Arrays.stream(namespaces.split(DEFAULT_SEPARATOR))
                .map(TextNamespace::new)
                .collect(Collectors.toList())
        );
    }

    @Override
    public String toString() {
        return namespaces
            .stream()
            .map(TextNamespace::toString)
            .collect(Collectors.joining(","));
    }
}

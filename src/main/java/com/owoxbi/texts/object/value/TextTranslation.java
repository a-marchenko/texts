package com.owoxbi.texts.object.value;

import com.fasterxml.jackson.annotation.JsonValue;
import lombok.NonNull;
import lombok.Value;

@Value
public class TextTranslation {
    @JsonValue
    @NonNull
    String translation;
}

package com.owoxbi.texts.component;

import com.owoxbi.texts.object.value.TextNamespaces;
import lombok.NonNull;

import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class TextQueryComponent {

    public static String compute(@NonNull TextNamespaces namespaces) {
        return namespaces
            .getNamespaces()
            .stream()
            .map(namespace -> "^" + Pattern.quote(namespace.getNamespace()) + "(\\.|$)")
            .collect(Collectors.joining("|"));
    }
}

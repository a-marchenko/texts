package com.owoxbi.texts.component;

import com.owoxbi.texts.dao.entity.TextAliasEntity;
import com.owoxbi.texts.object.value.Text;
import com.owoxbi.texts.object.value.TextNamespace;
import lombok.NonNull;

import java.util.List;
import java.util.regex.Pattern;

public class TextReplacementComponent {

    private List<TextAliasEntity> textAliasEntityList;

    public TextReplacementComponent(@NonNull List<TextAliasEntity> textAliasEntityList) {
        this.textAliasEntityList = textAliasEntityList;
    }

    public Text replace(@NonNull Text text) {
        final TextAliasEntity textAliasEntity = textAliasEntityList
            .stream()
            .filter(entity -> text.getNamespace().isSub(entity.getRefersToName()))
            .findAny()
            .orElse(null);
        return null == textAliasEntity ? text : replace(text, textAliasEntity);
    }

    private Text replace(Text text, TextAliasEntity textAliasEntity) {
        return new Text(
            new TextNamespace(
                text.getNamespace().getNamespace().replaceFirst(
                    Pattern.quote(textAliasEntity.getRefersToName()),
                    textAliasEntity.getName()
                )
            ),
            text.getTranslation()
        );
    }
}

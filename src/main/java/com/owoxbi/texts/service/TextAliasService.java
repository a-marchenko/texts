package com.owoxbi.texts.service;

import com.owoxbi.texts.dao.entity.TextAliasEntity;
import com.owoxbi.texts.object.value.TextNamespaces;
import reactor.core.publisher.Flux;

public interface TextAliasService {
    Flux<TextAliasEntity> fetchAll(TextNamespaces namespaces);
}

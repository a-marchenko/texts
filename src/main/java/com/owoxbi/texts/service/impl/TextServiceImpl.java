package com.owoxbi.texts.service.impl;

import com.owoxbi.texts.component.TextReplacementComponent;
import com.owoxbi.texts.object.value.Language;
import com.owoxbi.texts.object.value.Text;
import com.owoxbi.texts.object.value.TextNamespace;
import com.owoxbi.texts.object.value.TextNamespaces;
import com.owoxbi.texts.service.TextAliasService;
import com.owoxbi.texts.service.TextService;
import com.owoxbi.texts.service.TextTranslationService;
import com.owoxbi.texts.service.exception.EmptyTextAliasException;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;

import java.util.stream.Collectors;

@Service
public class TextServiceImpl implements TextService {
    private final TextTranslationService textTranslationService;

    private final TextAliasService textAliasService;

    public TextServiceImpl(TextTranslationService textTranslationService, TextAliasService textAliasService) {
        this.textTranslationService = textTranslationService;
        this.textAliasService = textAliasService;
    }

    @Override
    public Flux<Text> fetchAll(TextNamespaces namespaces, Language language) {
        return textAliasService
            .fetchAll(namespaces)
            .switchIfEmpty(Flux.error(EmptyTextAliasException::new))
            .collectList()
            .map(textAliasEntityList -> {
                final TextNamespaces textNamespaces = new TextNamespaces(
                    textAliasEntityList.stream()
                        .map(textAliasEntity -> new TextNamespace(textAliasEntity.getRefersToName()))
                        .collect(Collectors.toList())
                );
                final TextReplacementComponent textReplacement = new TextReplacementComponent(textAliasEntityList);
                return textTranslationService.fetchAll(textNamespaces, language).map(textReplacement::replace);
            })
            .flatMapMany(textFluxFromAlias -> textFluxFromAlias)
            .mergeWith(textTranslationService.fetchAll(namespaces, language))
            .onErrorResume(EmptyTextAliasException.class, e -> textTranslationService.fetchAll(namespaces, language));

    }
}
package com.owoxbi.texts.service.impl;

import com.owoxbi.texts.component.TextQueryComponent;
import com.owoxbi.texts.dao.TextTranslationRepository;
import com.owoxbi.texts.object.value.Language;
import com.owoxbi.texts.object.value.Text;
import com.owoxbi.texts.object.value.TextNamespaces;
import com.owoxbi.texts.service.TextTranslationService;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;

@Service
public class TextTranslationServiceImpl implements TextTranslationService {

    private final TextTranslationRepository textTranslationRepository;

    public TextTranslationServiceImpl(TextTranslationRepository textTranslationRepository) {
        this.textTranslationRepository = textTranslationRepository;
    }

    @Override
    public Flux<Text> fetchAll(TextNamespaces namespaces, Language language) {
        return textTranslationRepository.findAllByDeleteIsFalseAndNameRegex(TextQueryComponent.compute(namespaces))
            .map(textEntity -> Text.from(textEntity, language));
    }
}

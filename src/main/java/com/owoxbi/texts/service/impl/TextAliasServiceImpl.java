package com.owoxbi.texts.service.impl;

import com.owoxbi.texts.component.TextQueryComponent;
import com.owoxbi.texts.dao.TextAliasRepository;
import com.owoxbi.texts.dao.entity.TextAliasEntity;
import com.owoxbi.texts.object.value.TextNamespaces;
import com.owoxbi.texts.service.TextAliasService;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;

@Service
public class TextAliasServiceImpl implements TextAliasService {

    private final TextAliasRepository textAliasRepository;

    public TextAliasServiceImpl(TextAliasRepository textAliasRepository) {
        this.textAliasRepository = textAliasRepository;
    }

    @Override
    public Flux<TextAliasEntity> fetchAll(TextNamespaces namespaces) {
        return textAliasRepository.findAllByDeleteIsFalseAndNameRegex(TextQueryComponent.compute(namespaces));
    }
}

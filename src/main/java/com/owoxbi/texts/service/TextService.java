package com.owoxbi.texts.service;

import com.owoxbi.texts.object.value.Language;
import com.owoxbi.texts.object.value.Text;
import com.owoxbi.texts.object.value.TextNamespaces;
import reactor.core.publisher.Flux;

public interface TextService {
    Flux<Text> fetchAll(TextNamespaces namespaces, Language language);
}

package com.owoxbi.texts.controller;

import com.owoxbi.texts.object.value.*;
import com.owoxbi.texts.service.TextService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import java.util.Map;

@Slf4j
@RestController
@RequestMapping("api/v1/texts")
public class TextController {

    private final TextService textService;

    public TextController(@Autowired TextService textService) {
        this.textService = textService;
    }

    @GetMapping
    public Mono<Map<TextNamespace, TextTranslation>> getAllBy(
        @RequestParam("namespaces") String namespaces,
        @RequestParam("language") String language
    ) {
        return textService.fetchAll(TextNamespaces.from(namespaces), Language.from(language))
            .collectMap(Text::getNamespace, Text::getTranslation);
    }
}

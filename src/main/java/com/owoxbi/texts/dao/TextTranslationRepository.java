package com.owoxbi.texts.dao;

import com.owoxbi.texts.dao.entity.TextEntity;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;

@Repository
public interface TextTranslationRepository extends ReactiveMongoRepository<TextEntity, String> {
    Flux<TextEntity> findAllByDeleteIsFalseAndNameRegex(String namespaces);
}

package com.owoxbi.texts.dao;

import com.owoxbi.texts.dao.entity.TextAliasEntity;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;

@Repository
public interface TextAliasRepository extends ReactiveMongoRepository<TextAliasEntity, String> {
    Flux<TextAliasEntity> findAllByDeleteIsFalseAndNameRegex(String namespaces);
}

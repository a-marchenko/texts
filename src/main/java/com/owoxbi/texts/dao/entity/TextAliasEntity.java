package com.owoxbi.texts.dao.entity;

import lombok.Builder;
import lombok.Data;
import lombok.NonNull;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Data
@Document(collection = "texts-aliases")
@Builder
public class TextAliasEntity {

    @NonNull
    @Indexed(unique = true)
    private String name;

    @Field("refers_to")
    @Indexed
    private String refersToName;

    @Id
    @Field
    private String id;

    // TODO make LocalDateTime
    @CreatedDate
    @Field("created")
    private String createdDate;

    // TODO make LocalDateTime
    @LastModifiedDate
    @Field("changed")
    private String changedDate;

    @Field
    @NonNull
    @Builder.Default
    private Boolean delete = Boolean.FALSE;
}


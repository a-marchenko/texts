package com.owoxbi.texts.dao.entity;

import lombok.Data;
import lombok.Builder;
import lombok.NonNull;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Data
@Document(collection = "texts")
@Builder
public class TextEntity {

    @NonNull
    @Indexed(unique = true)
    @Field
    private String name;

    @Field("text_stub")
    private String stub;

    @Field
    private String ru;

    @Field
    private String en;

    @Id
    @Field
    protected String id;

    // TODO make LocalDateTime
    @CreatedDate()
    @Field("created")
    protected String createdDate;

    // TODO make LocalDateTime
    @LastModifiedDate
    @Field("changed")
    protected String changedDate;

    @Field
    @NonNull
    @Builder.Default
    protected Boolean delete = Boolean.FALSE;
}

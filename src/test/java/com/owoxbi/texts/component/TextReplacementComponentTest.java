package com.owoxbi.texts.component;

import com.owoxbi.texts.dao.entity.TextAliasEntity;
import com.owoxbi.texts.object.value.Text;
import com.owoxbi.texts.object.value.TextNamespace;
import com.owoxbi.texts.object.value.TextTranslation;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TextReplacementComponentTest {

    @Test
    public void shouldTrowNullPointerExceptionWithNullTextAliasEntityListParam() {
        assertThatThrownBy(() -> new TextReplacementComponent(null))
            .isInstanceOf(NullPointerException.class)
            .hasMessage("textAliasEntityList is marked @NonNull but is null");
    }

    @Test
    public void shouldTrowNullPointerExceptionWithNullTextParam() {
        final List<TextAliasEntity> textAliasEntityList = List.of(TextAliasEntity.builder().name("A").build());
        final TextReplacementComponent textReplacementComponent = new TextReplacementComponent(textAliasEntityList);

        assertThatThrownBy(() -> textReplacementComponent.replace(null))
            .isInstanceOf(NullPointerException.class)
            .hasMessage("text is marked @NonNull but is null");
    }

    @Test
    public void shouldTrowNullPointerExceptionWithEmptyTextAliasEntity() {
        final List<TextAliasEntity> textAliasEntityList = List.of(TextAliasEntity.builder().name("A").build());
        final TextReplacementComponent textReplacementComponent = new TextReplacementComponent(textAliasEntityList);

        final Text text1 = new Text(new TextNamespace("Aa"), new TextTranslation("Some"));
        assertThatThrownBy(() -> textReplacementComponent.replace(text1))
            .isInstanceOf(NullPointerException.class)
            .hasMessage("textNamespace is marked @NonNull but is null");
    }

    @Test
    public void shouldReturnTextWithoutReplace() {
        final TextAliasEntity textAliasEntity = TextAliasEntity.builder().name("A").build();
        textAliasEntity.setName("Bb");
        textAliasEntity.setRefersToName("Cc");

        final List<TextAliasEntity> textAliasEntityList = List.of(textAliasEntity);
        final TextReplacementComponent textReplacementComponent = new TextReplacementComponent(textAliasEntityList);

        final Text text1 = new Text(new TextNamespace("Aa"), new TextTranslation("Some"));
        assertThat(textReplacementComponent.replace(text1)).isSameAs(text1);
    }

    @Test
    public void shouldReturnTextWithFullReplace() {
        final String namespace1 = "Aa";
        final String namespace2 = "Bb";

        final TextAliasEntity textAliasEntity = TextAliasEntity.builder().name("A").build();
        textAliasEntity.setName(namespace2);
        textAliasEntity.setRefersToName(namespace1);

        final List<TextAliasEntity> textAliasEntityList = List.of(textAliasEntity);
        final TextReplacementComponent textReplacementComponent = new TextReplacementComponent(textAliasEntityList);

        final TextTranslation textTranslation = new TextTranslation("Some");

        final Text text1 = new Text(new TextNamespace(namespace1), textTranslation);

        assertThat(textReplacementComponent.replace(text1))
            .isEqualTo(new Text(new TextNamespace(namespace2), textTranslation));
    }
}

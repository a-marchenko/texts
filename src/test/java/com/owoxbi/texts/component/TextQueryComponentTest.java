package com.owoxbi.texts.component;

import com.owoxbi.texts.object.value.TextNamespace;
import com.owoxbi.texts.object.value.TextNamespaces;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TextQueryComponentTest {

    private static final String REGEXP_SUFFIX = "\\E(\\.|$)";
    private static final String REGEXP_PREFIX = "^\\Q";

    @Test
    public void shouldTrowNullPointerExceptionWithNullNamespaces() {
        assertThatThrownBy(() -> TextQueryComponent.compute(null))
            .isInstanceOf(NullPointerException.class)
            .hasMessage("namespaces is marked @NonNull but is null");
    }

    @Test
    public void shouldReturnEmptyStringWithEmptyNamespaces() {
        assertThat(TextQueryComponent.compute(new TextNamespaces(List.of()))).isEmpty();
    }

    @Test
    public void shouldReturnRegExpFroOneNamespace() {
        final String namespace = "Aa";
        assertThat(TextQueryComponent.compute(new TextNamespaces(List.of(new TextNamespace(namespace)))))
            .isEqualTo(REGEXP_PREFIX + namespace + REGEXP_SUFFIX);
    }

    @Test
    public void shouldReturnRegExpFroManyNamespace() {
        final String namespace1 = "Aa";
        final String namespace2 = "Bb.Bb1";
        final String namespace3 = "Cc.Cc1.Cc2";

        final List<TextNamespace> namespaces = List.of(
            new TextNamespace(namespace1), new TextNamespace(namespace2), new TextNamespace(namespace3)
        );

        assertThat(TextQueryComponent.compute(new TextNamespaces(namespaces)))
            .isEqualTo(REGEXP_PREFIX + namespace1 + REGEXP_SUFFIX + "|" + REGEXP_PREFIX
                + namespace2 + REGEXP_SUFFIX + "|" + REGEXP_PREFIX + namespace3 + REGEXP_SUFFIX);
    }
}

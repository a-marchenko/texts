package com.owoxbi.texts.object.value;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TextNamespacesTest {
    @Test
    public void shouldTrowNullPointerExceptionWithNullNamespaces() {
        assertThatThrownBy(() -> TextNamespaces.from(null))
            .isInstanceOf(NullPointerException.class)
            .hasMessage("namespaces is marked @NonNull but is null");

        assertThatThrownBy(() -> new TextNamespaces(null))
            .isInstanceOf(NullPointerException.class)
            .hasMessage("namespaces is marked @NonNull but is null");
    }

    @Test
    public void shouldCreateFromStaticCorrectOneNamespace() {
        final String namespace = "Aa";
        final TextNamespaces textNamespaces = TextNamespaces.from(namespace);

        assertThat(textNamespaces.getNamespaces()).isEqualTo(List.of(new TextNamespace(namespace)));
        assertThat(textNamespaces.toString()).isEqualTo(namespace);
    }

    @Test
    public void shouldConstruct() {
        final String namespace = "Aa";
        final List<TextNamespace> textNamespaceList = List.of(new TextNamespace(namespace));
        final TextNamespaces textNamespaces = new TextNamespaces(textNamespaceList);

        assertThat(textNamespaces.getNamespaces()).isSameAs(textNamespaceList);
        assertThat(textNamespaces.toString()).isEqualTo(namespace);
    }

    @Test
    public void shouldCreateFromStaticCorrectManyNamespace() {
        final String namespace1 = "Aa.Aa1";
        final String namespace2 = "Bb";
        final String namespace3 = "Cc.Cc1";

        final String namespace = namespace1 + "," + namespace2 + "," + namespace3;
        final TextNamespaces textNamespaces = TextNamespaces.from(namespace);

        final List<TextNamespace> textNamespaceList = List.of(
            new TextNamespace(namespace1), new TextNamespace(namespace2), new TextNamespace(namespace3)
        );
        assertThat(textNamespaces.getNamespaces()).isEqualTo(textNamespaceList);
        assertThat(textNamespaces.toString()).isEqualTo(namespace);
    }
}

package com.owoxbi.texts.object.value;

import com.owoxbi.texts.dao.entity.TextEntity;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TextTest {
    @Test
    public void shouldTrowNullPointerExceptionWithNullTextEntity() {
        assertThatThrownBy(() -> Text.from(null, null))
            .isInstanceOf(NullPointerException.class)
            .hasMessage("textEntity is marked @NonNull but is null");
    }

    @Test
    public void shouldTrowNullPointerExceptionWithNullLanguage() {
        assertThatThrownBy(() -> Text.from(TextEntity.builder().name("A").build(), null))
            .isInstanceOf(NullPointerException.class)
            .hasMessage("language is marked @NonNull but is null");
    }

    @Test
    public void shouldTrowNullPointerExceptionWithNullTextNamespace() {
        assertThatThrownBy(() -> new Text(null, null))
            .isInstanceOf(NullPointerException.class)
            .hasMessage("namespace is marked @NonNull but is null");
    }

    @Test
    public void shouldTrowNullPointerExceptionWithNullTextTranslation() {
        assertThatThrownBy(() -> new Text(new TextNamespace("Aa"), null))
            .isInstanceOf(NullPointerException.class)
            .hasMessage("translation is marked @NonNull but is null");
    }

    @Test
    public void shouldConstructed() {
        final TextNamespace textNamespace = new TextNamespace("A");
        final TextTranslation textTranslation = new TextTranslation("Some");
        final Text text = new Text(textNamespace, textTranslation);

        assertThat(text.getNamespace()).isSameAs(textNamespace);
        assertThat(text.getTranslation()).isSameAs(textTranslation);
    }

    @Test
    public void shouldBeCreatedFromStaticConstructor() {
        final String namespace = "A";
        final String translation = "a";
        final Text text1 = Text.from(TextEntity.builder().name(namespace).ru(translation).build(), Language.ru);

        assertThat(text1.getNamespace()).isEqualTo(new TextNamespace(namespace));
        assertThat(text1.getTranslation()).isEqualTo(new TextTranslation(translation));

        final Text text2 = Text.from(TextEntity.builder().name(namespace).en(translation).build(), Language.en);

        assertThat(text2.getNamespace()).isEqualTo(new TextNamespace(namespace));
        assertThat(text2.getTranslation()).isEqualTo(new TextTranslation(translation));

        final String stub = "stub";
        final Text text3 = Text.from(TextEntity.builder().name(namespace).stub(stub).build(), Language.en);

        assertThat(text3.getNamespace()).isEqualTo(new TextNamespace(namespace));
        assertThat(text3.getTranslation()).isEqualTo(new TextTranslation(stub));
    }

    @Test
    public void shouldBeTrowException() {
        assertThatThrownBy(() -> Text.from(TextEntity.builder().name("A").build(), Language.stub))
            .isInstanceOf(IllegalArgumentException.class)
            .hasMessage("Unknown language: " + Language.stub.name());
    }
}

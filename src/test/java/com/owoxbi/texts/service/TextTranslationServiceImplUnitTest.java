package com.owoxbi.texts.service;

import com.owoxbi.texts.dao.TextTranslationRepository;
import com.owoxbi.texts.dao.entity.TextEntity;
import com.owoxbi.texts.object.value.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TextTranslationServiceImplUnitTest {

    @Autowired
    private TextTranslationService textTranslationService;

    @MockBean
    private TextTranslationRepository textTranslationRepository;

    @Test
    public void testFetchAllNotEmpty() {
        final var namespace1 = "Aa.Aa";
        final var namespace2 = "Aa.Bb";
        final var namespace3 = "Aa.Cc";
        final var translation1 = "en1";
        final var translation2 = "en2";
        final var stub3 = "s3";

        when(textTranslationRepository.findAllByDeleteIsFalseAndNameRegex(any(String.class))).thenReturn(Flux.just(
            TextEntity.builder().name(namespace1).ru("ru1").en(translation1).stub("s1").build(),
            TextEntity.builder().name(namespace2).ru("ru2").en(translation2).stub("s2").build(),
            TextEntity.builder().name(namespace3).ru("ru3").stub(stub3).build()
        ));

        final Flux<Text> textFlux = textTranslationService
            .fetchAll(new TextNamespaces(List.of(new TextNamespace("Aa"))), Language.en);

        assertThat(textFlux).isNotNull().isInstanceOf(Flux.class);

        StepVerifier.create(textFlux)
            .consumeNextWith((Text text) -> {
                assertThat(text).isNotNull();
                assertThat(text.getNamespace()).isEqualTo(new TextNamespace(namespace1));
                assertThat(text.getTranslation()).isEqualTo(new TextTranslation(translation1));
            })
            .consumeNextWith((Text text) -> {
                assertThat(text).isNotNull();
                assertThat(text.getNamespace()).isEqualTo(new TextNamespace(namespace2));
                assertThat(text.getTranslation()).isEqualTo(new TextTranslation(translation2));
            })
            .consumeNextWith((Text text) -> {
                assertThat(text).isNotNull();
                assertThat(text.getNamespace()).isEqualTo(new TextNamespace(namespace3));
                assertThat(text.getTranslation()).isEqualTo(new TextTranslation(stub3));
            })
            .expectNextCount(0)
            .verifyComplete();
    }
}

package com.owoxbi.texts.service;

import com.owoxbi.texts.dao.TextAliasRepository;
import com.owoxbi.texts.dao.entity.TextAliasEntity;
import com.owoxbi.texts.object.value.TextNamespace;
import com.owoxbi.texts.object.value.TextNamespaces;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TextAliasServiceImplUnitTest {

    @Autowired
    private TextAliasService textAliasService;

    @MockBean
    private TextAliasRepository textAliasRepository;

    @Test
    public void testFetchAllNotEmpty() {
        final var textAliasEntity1 = TextAliasEntity.builder().name("Aa").refersToName("Cc.Dd").delete(false).build();
        final var textAliasEntity2 = TextAliasEntity.builder().name("Bb").refersToName("Ee.Ff").delete(false).build();

        final Flux<TextAliasEntity> textAliasEntityFlux = Flux.just(textAliasEntity1, textAliasEntity2);
        when(textAliasRepository.findAllByDeleteIsFalseAndNameRegex(any(String.class))).thenReturn(textAliasEntityFlux);

        final var textNamespaces = new TextNamespaces(List.of(new TextNamespace("Aa")));
        final Flux<TextAliasEntity> textEntities = textAliasService.fetchAll(textNamespaces);
        assertThat(textEntities).isNotNull().isInstanceOf(Flux.class);

        StepVerifier.create(textEntities)
            .expectNext(textAliasEntity1)
            .expectNext(textAliasEntity2)
            .expectNextCount(0)
            .verifyComplete();
    }
}

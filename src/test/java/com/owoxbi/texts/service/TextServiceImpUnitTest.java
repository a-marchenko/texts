package com.owoxbi.texts.service;

import com.owoxbi.texts.dao.entity.TextAliasEntity;
import com.owoxbi.texts.object.value.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TextServiceImpUnitTest {

    @MockBean
    private TextTranslationService textTranslationService;

    @MockBean
    private TextAliasService textAliasService;

    @Autowired
    private TextService textService;

    @Test
    public void testFetchAllTranslationsNotExistsAndAliasesNotExists() {
        TextNamespaces textNamespaces = new TextNamespaces(List.of(new TextNamespace("Some")));
        Language language = Language.ru;

        when(textAliasService.fetchAll(textNamespaces)).thenReturn(Flux.empty());
        when(textTranslationService.fetchAll(textNamespaces, language)).thenReturn(Flux.empty());

        Flux<Text> textFlux = textService.fetchAll(textNamespaces, language);

        StepVerifier.create(textFlux).expectNextCount(0).verifyComplete();
    }

    @Test
    public void testFetchAllTranslationsExistsAndAliasesNotExists() {
        TextNamespaces textNamespaces = new TextNamespaces(List.of(new TextNamespace("Aa")));
        Language language = Language.ru;

        Text text1 = new Text(new TextNamespace("Aa.Aa1"), new TextTranslation("Some"));

        when(textAliasService.fetchAll(textNamespaces)).thenReturn(Flux.empty());
        when(textTranslationService.fetchAll(textNamespaces, language)).thenReturn(Flux.just(text1));

        Flux<Text> textFlux = textService.fetchAll(textNamespaces, language);

        StepVerifier.create(textFlux)
            .consumeNextWith((Text text) -> assertThat(text).isEqualTo(text1))
            .expectNextCount(0)
            .verifyComplete();
    }

    @Test
    public void testFetchAllTranslationsNotExistsAndAliasesExistsAndTranslationsForAliasesNotExists() {
        String refersToName = "Aa1";
        TextNamespaces textNamespaces = new TextNamespaces(List.of(new TextNamespace("Aa")));
        TextNamespaces textNamespacesFromAlias = new TextNamespaces(List.of(new TextNamespace(refersToName)));
        Language language = Language.ru;

        TextAliasEntity textAliasEntity = createStubTextAliasEntity("Aa", refersToName);

        when(textAliasService.fetchAll(textNamespaces)).thenReturn(Flux.just(textAliasEntity));
        when(textTranslationService.fetchAll(textNamespaces, language)).thenReturn(Flux.empty());
        when(textTranslationService.fetchAll(textNamespacesFromAlias, language)).thenReturn(Flux.empty());

        Flux<Text> textFlux = textService.fetchAll(textNamespaces, language);

        StepVerifier.create(textFlux).expectNextCount(0).verifyComplete();
    }

    @Test
    public void testFetchAllTranslationsExistsAndAliasesExistsAndTranslationsForAliasesNotExists() {
        String refersToName = "Aa1";
        TextNamespaces textNamespaces = new TextNamespaces(List.of(new TextNamespace("Aa")));
        TextNamespaces textNamespacesFromAlias = new TextNamespaces(List.of(new TextNamespace(refersToName)));
        Language language = Language.ru;

        TextAliasEntity textAliasEntity = createStubTextAliasEntity("Aa", refersToName);
        Text text1 = new Text(new TextNamespace("Aa.Bb"), new TextTranslation("Some"));

        when(textAliasService.fetchAll(textNamespaces)).thenReturn(Flux.just(textAliasEntity));
        when(textTranslationService.fetchAll(textNamespaces, language)).thenReturn(Flux.just(text1));
        when(textTranslationService.fetchAll(textNamespacesFromAlias, language)).thenReturn(Flux.empty());

        Flux<Text> textFlux = textService.fetchAll(textNamespaces, language);

        StepVerifier.create(textFlux)
            .consumeNextWith((Text text) -> assertThat(text).isEqualTo(text1))
            .expectNextCount(0)
            .verifyComplete();
    }

    @Test
    public void testFetchAllTranslationsNotExistsAndAliasesExistsAndTranslationsForAliasesExists() {
        String refersToName = "Aa1";
        String textName = "Aa";
        TextNamespaces textNamespaces = new TextNamespaces(List.of(new TextNamespace(textName)));
        TextNamespaces textNamespacesFromAlias = new TextNamespaces(List.of(new TextNamespace(refersToName)));
        Language language = Language.ru;

        TextAliasEntity textAliasEntity = createStubTextAliasEntity(textName, refersToName);

        TextTranslation textTranslation = new TextTranslation("Some");
        Text textForAlias1 = new Text(new TextNamespace(refersToName), textTranslation);

        when(textAliasService.fetchAll(textNamespaces)).thenReturn(Flux.just(textAliasEntity));
        when(textTranslationService.fetchAll(textNamespaces, language)).thenReturn(Flux.empty());
        when(textTranslationService.fetchAll(textNamespacesFromAlias, language)).thenReturn(Flux.just(textForAlias1));

        Text expectedText = new Text(new TextNamespace(textName), textTranslation);

        Flux<Text> textFlux = textService.fetchAll(textNamespaces, language);

        StepVerifier.create(textFlux)
            .consumeNextWith((Text text) -> assertThat(text).isEqualTo(expectedText))
            .expectNextCount(0)
            .verifyComplete();
    }

    @Test
    public void testFetchAllTranslationsExistsAndAliasesExistsAndTranslationsForAliasesExists() {
        String refersToName = "Bb.Dd";
        TextAliasEntity textAliasEntity = createStubTextAliasEntity("Aa1.Bb1", refersToName);

        Language language = Language.en;
        TextNamespaces textNamespaces = new TextNamespaces(
            List.of(new TextNamespace("Aa.Bb"), new TextNamespace("Aa1.Bb1"))
        );

        when(textAliasService.fetchAll(textNamespaces)).thenReturn(Flux.just(textAliasEntity));

        Text text1 = new Text(new TextNamespace("Aa.Bb.Cc"), new TextTranslation("Some"));
        Text text2 = new Text(new TextNamespace("Aa.Bb.Cc1"), new TextTranslation("Some1"));

        TextTranslation translationForAlias1 = new TextTranslation("From Alias1");
        TextTranslation translationForAlias2 = new TextTranslation("From Alias2");
        Text textForAliases1 = new Text(new TextNamespace("Bb.Dd.Ee1"), translationForAlias1);
        Text textForAliases2 = new Text(new TextNamespace("Bb.Dd.Ee2"), translationForAlias2);

        when(textTranslationService.fetchAll(textNamespaces, language)).thenReturn(Flux.just(text1, text2));
        when(textTranslationService.fetchAll(new TextNamespaces(List.of(new TextNamespace(refersToName))), language))
            .thenReturn(Flux.just(textForAliases1, textForAliases2));

        Flux<Text> textFlux = textService.fetchAll(textNamespaces, language);

        assertThat(textFlux).isNotNull().isInstanceOf(Flux.class);

        Text textFromAlias1 = new Text(new TextNamespace("Aa1.Bb1.Ee1"), translationForAlias1);
        Text textFromAlias2 = new Text(new TextNamespace("Aa1.Bb1.Ee2"), translationForAlias2);

        StepVerifier.create(textFlux)
            .consumeNextWith((Text text) -> assertThat(text).isEqualTo(textFromAlias1))
            .consumeNextWith((Text text) -> assertThat(text).isEqualTo(textFromAlias2))
            .consumeNextWith((Text text) -> assertThat(text).isEqualTo(text1))
            .consumeNextWith((Text text) -> assertThat(text).isEqualTo(text2))
            .expectNextCount(0)
            .verifyComplete();
    }

    private TextAliasEntity createStubTextAliasEntity(String name, String refersToName) {
        return TextAliasEntity.builder().name(name).refersToName(refersToName).delete(false).build();
    }
}

package com.owoxbi.texts;

import com.owoxbi.texts.controller.TextController;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TextsApplicationTests {

    @Autowired
    private TextController textController;

    @Test
    public void smokeTest() {
        assertThat(textController).isNotNull().isInstanceOf(TextController.class);
    }
}

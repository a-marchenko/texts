package com.owoxbi.texts.dao;

import com.owoxbi.texts.component.TextQueryComponent;
import com.owoxbi.texts.dao.entity.TextEntity;
import com.owoxbi.texts.object.value.TextNamespaces;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.test.context.junit4.SpringRunner;
import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

import java.util.stream.Stream;

@RunWith(SpringRunner.class)
@DataMongoTest
public class TextTranslationRepositoryTest {

    @Autowired
    private TextTranslationRepository textTranslationRepository;

    @Test
    public void testWithOneNamespace() {
        final TextEntity[] satisfiedQuery = {
            TextEntity.builder().name("Aa").stub("s01").en("En01").ru("Ru01").build(),
            TextEntity.builder().name("Aa.Aa11").stub("s11").en("En11").ru("Ru11").build(),
            TextEntity.builder().name("Aa.Aa12").stub("s12").en("En12").ru("Ru12").delete(false).build()
        };

        final TextEntity[] unSatisfiedQuery = {
            TextEntity.builder().name("Aa.Aa1").stub("s1").en("En1").ru("Ru1").delete(true).build(),
            TextEntity.builder().name("Bb").stub("s2").en("En2").ru("Ru2").build(),
            TextEntity.builder().name("Bb.Aa").stub("s3").en("En3").ru("Ru3").build(),
            TextEntity.builder().name("Aa1.Aa").stub("s4").en("En4").ru("Ru4").build()
        };

        final Flux<TextEntity> findResult1 = textTranslationRepository.findAllByDeleteIsFalseAndNameRegex(
            TextQueryComponent.compute(TextNamespaces.from("Aa"))
        );

        StepVerifier
            .create(Flux.from(setupRepository(satisfiedQuery, unSatisfiedQuery)).thenMany(findResult1))
            .expectNext(satisfiedQuery)
            .expectNextCount(0)
            .verifyComplete();

        final Flux<TextEntity> findResult2 = textTranslationRepository.findAllByDeleteIsFalseAndNameRegex(
            TextQueryComponent.compute(TextNamespaces.from("Aa.Bb.Cc"))
        );

        StepVerifier
            .create(Flux.from(findResult2))
            .expectNextCount(0)
            .verifyComplete();
    }

    @Test
    public void testWithManyNamespace() {
        final TextEntity[] satisfiedQuery = {


            TextEntity.builder().name("Aa.Aa12").stub("s12").en("En12").ru("Ru12").delete(false).build()
        };

        final TextEntity[] unSatisfiedQuery = {
            TextEntity.builder().name("Aa").stub("s01").en("En01").ru("Ru01").build(),
            TextEntity.builder().name("Aa.Bb1").stub("s11").en("En11").ru("Ru11").build(),
            TextEntity.builder().name("Bb").stub("s2").en("En2").ru("Ru2").build(),
            TextEntity.builder().name("Bb.Aa").stub("s3").en("En3").ru("Ru3").build(),
            TextEntity.builder().name("Aa1.Aa").stub("s4").en("En4").ru("Ru4").build()
        };

        final Flux<TextEntity> findResult1 = textTranslationRepository.findAllByDeleteIsFalseAndNameRegex(
            TextQueryComponent.compute(TextNamespaces.from("Aa.Bb"))
        );

        StepVerifier
            .create(Flux.from(setupRepository(satisfiedQuery, unSatisfiedQuery)).thenMany(findResult1))
            .expectNext(satisfiedQuery)
            .expectNextCount(0)
            .verifyComplete();

        final Flux<TextEntity> findResult2 = textTranslationRepository.findAllByDeleteIsFalseAndNameRegex(
            TextQueryComponent.compute(TextNamespaces.from("Aa.Bb.Cc"))
        );

        StepVerifier
            .create(Flux.from(findResult2))
            .expectNextCount(0)
            .verifyComplete();
    }

    private Flux<TextEntity> setupRepository(final TextEntity[] satisfiedQuery, final TextEntity[] unSatisfiedQuery) {
        return textTranslationRepository
            .deleteAll()
            .thenMany(textTranslationRepository.saveAll(
                Flux.just(Stream.of(satisfiedQuery, unSatisfiedQuery).flatMap(Stream::of).toArray(TextEntity[]::new)))
            );
    }
}

package com.owoxbi.texts.dao;

import com.owoxbi.texts.component.TextQueryComponent;
import com.owoxbi.texts.dao.entity.TextAliasEntity;
import com.owoxbi.texts.object.value.TextNamespaces;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.test.context.junit4.SpringRunner;
import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

import java.util.stream.Stream;

@RunWith(SpringRunner.class)
@DataMongoTest
public class TextAliasRepositoryTest {

    @Autowired
    private TextAliasRepository textAliasRepository;

    @Test
    public void testWithOneNamespace() {
        final TextAliasEntity[] satisfiedQuery = {
            TextAliasEntity.builder().name("Aa.Bb").refersToName("Cc").delete(false).build(),
            TextAliasEntity.builder().name("Aa.Bb.Cc").refersToName("Bb.Cc").delete(false).build(),
            TextAliasEntity.builder().name("Aa.Bb.Ee").refersToName("Bb.Ee").build(),
        };

        final TextAliasEntity[] unSatisfiedQuery = {
            TextAliasEntity.builder().name("Aa.Bb.Bb1").refersToName("Bb").delete(true).build(),
            TextAliasEntity.builder().name("Aa.BB").refersToName("BB").delete(false).build(),
            TextAliasEntity.builder().name("Aa").refersToName("BB").build(),
        };

        final Flux<TextAliasEntity> findResult = textAliasRepository.findAllByDeleteIsFalseAndNameRegex(
            TextQueryComponent.compute(TextNamespaces.from("Aa.Bb"))
        );

        StepVerifier
            .create(Flux.from(setup(satisfiedQuery, unSatisfiedQuery)).thenMany(findResult))
            .expectNext(satisfiedQuery)
            .expectNextCount(0)
            .verifyComplete();
    }

    private Flux<TextAliasEntity> setup(TextAliasEntity[] satisfiedQuery, TextAliasEntity[] unSatisfiedQuery) {
        return textAliasRepository
            .deleteAll()
            .thenMany(textAliasRepository.saveAll(Flux.just(
                Stream.of(satisfiedQuery, unSatisfiedQuery).flatMap(Stream::of).toArray(TextAliasEntity[]::new))
            ));
    }
}

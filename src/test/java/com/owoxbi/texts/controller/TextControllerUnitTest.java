package com.owoxbi.texts.controller;

import com.owoxbi.texts.object.value.*;
import com.owoxbi.texts.service.TextService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TextControllerUnitTest {

    @Autowired
    private TextController textController;

    @MockBean
    private TextService textService;

    @Test
    public void testGetAllBy_withEmptyResult() {
        final Language language = Language.ru;
        final String namespace = "Aa";
        when(textService.fetchAll(new TextNamespaces(List.of(new TextNamespace(namespace))), language))
            .thenReturn(Flux.just());

        StepVerifier.create(textController.getAllBy(namespace, language.name()))
            .consumeNextWith(textMap -> Assert.assertTrue(textMap.isEmpty()))
            .expectNextCount(0)
            .verifyComplete();
    }

    @Test
    public void testGetAllBy_withOneText() {
        final Language language = Language.ru;
        final String namespace = "Aa";
        final TextNamespace textNamespace = new TextNamespace(namespace);
        final TextTranslation textTranslation = new TextTranslation("Some");

        when(textService.fetchAll(new TextNamespaces(List.of(textNamespace)), language))
            .thenReturn(Flux.just(new Text(textNamespace, textTranslation)));

        StepVerifier.create(textController.getAllBy(namespace, language.name()))
            .consumeNextWith((Map<TextNamespace, TextTranslation> textMap) -> {
                assertThat(textMap.size()).isEqualTo(1);
                assertThat(textMap.get(textNamespace)).isNotNull().isSameAs(textTranslation);
            })
            .expectNextCount(0)
            .verifyComplete();
    }
}

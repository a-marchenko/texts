package com.owoxbi.texts.controller;

import com.owoxbi.texts.object.value.*;
import com.owoxbi.texts.service.TextService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import reactor.core.publisher.Flux;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.asyncDispatch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class TextControllerIntegrationTest {

    private static final String TEXTS_ENDPOINT = "/api/v1/texts/";

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private TextService textService;

    @Test
    public void testWithoutGetParamsShouldBeBadRequest() throws Exception {
        final var result = this.mockMvc.perform(get(TEXTS_ENDPOINT))
            .andDo(print())
            .andExpect(content().string(""))
            .andExpect(status().isBadRequest())
            .andExpect(status().reason("Required String parameter 'namespaces' is not present"))
            .andReturn();

        assertThat(result.getResponse().getHeaderNames()).isEqualTo(Collections.EMPTY_SET);
        assertThat(result.getResponse().getCookies().length).isZero();
        assertThat(result.getResponse().getContentType()).isNull();
    }

    @Test
    public void testWithoutGetParamLanguageShouldBeBadRequest() throws Exception {
        final var result = this.mockMvc.perform(get(TEXTS_ENDPOINT).param("namespaces", "Aa"))
            .andDo(print())
            .andExpect(content().string(""))
            .andExpect(status().isBadRequest())
            .andExpect(status().reason("Required String parameter 'language' is not present"))
            .andReturn();

        assertThat(result.getResponse().getHeaderNames()).isEqualTo(Collections.EMPTY_SET);
        assertThat(result.getResponse().getCookies().length).isZero();
        assertThat(result.getResponse().getContentType()).isNull();
    }

    @Test
    public void shouldReturnJsonWithOneText() throws Exception {
        final Language language = Language.ru;
        final String namespace = "Aa";
        final TextNamespace textNamespace = new TextNamespace(namespace);
        final String translation = "Some";

        final TextTranslation textTranslation = new TextTranslation(translation);

        when(textService.fetchAll(new TextNamespaces(List.of(textNamespace)), language))
            .thenReturn(Flux.just(new Text(textNamespace, textTranslation)));

        final var result = mockMvc.perform(
            get(TEXTS_ENDPOINT)
                .param("namespaces", namespace)
                .param("language", language.name())
        )
            .andDo(print())
            .andExpect(request().asyncStarted())
            .andExpect(request().asyncResult(instanceOf(HashMap.class)))
            .andReturn();

        mockMvc.perform(asyncDispatch(result))
            .andDo(print())
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
            .andExpect(jsonPath("Aa").value(translation));

        assertThat(result.getResponse().getHeader("Content-Type"))
            .isNotNull()
            .isEqualTo(MediaType.APPLICATION_JSON_UTF8.toString());
        assertThat(result.getResponse().getCookies().length).isZero();
    }

    @Test
    public void shouldReturnJsonWithManyText() throws Exception {
        final Language language = Language.ru;
        final String namespace = "Aa";

        final TextNamespace textNamespace = new TextNamespace(namespace);

        final String translation1 = "Some1";
        final String translation2 = "Some2";
        final String translation3 = "Some3";

        final TextTranslation textTranslation1 = new TextTranslation(translation1);
        final TextTranslation textTranslation2 = new TextTranslation(translation2);
        final TextTranslation textTranslation3 = new TextTranslation(translation3);

        final String textNamespace1 = namespace + ".Aa1";
        final String textNamespace2 = namespace + ".Aa2";
        final String textNamespace3 = namespace + ".Aa3";

        final Text text1 = new Text(new TextNamespace(textNamespace1), textTranslation1);
        final Text text2 = new Text(new TextNamespace(textNamespace2), textTranslation2);
        final Text text3 = new Text(new TextNamespace(textNamespace3), textTranslation3);

        when(textService.fetchAll(new TextNamespaces(List.of(textNamespace)), language))
            .thenReturn(Flux.just(text1, text2, text3));

        final var result = mockMvc.perform(
            get(TEXTS_ENDPOINT)
                .param("namespaces", namespace)
                .param("language", language.name())
        )
            .andDo(print())
            .andExpect(request().asyncStarted())
            .andExpect(request().asyncResult(instanceOf(HashMap.class)))
            .andReturn();

        mockMvc.perform(asyncDispatch(result))
            .andDo(print())
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
            .andExpect(jsonPath("['" + textNamespace1 + "']").value(translation1))
            .andExpect(jsonPath("['" + textNamespace2 + "']").value(translation2))
            .andExpect(jsonPath("['" + textNamespace3 + "']").value(translation3));

        assertThat(result.getResponse().getHeader("Content-Type"))
            .isNotNull()
            .isEqualTo(MediaType.APPLICATION_JSON_UTF8.toString());
        assertThat(result.getResponse().getCookies().length).isZero();
    }
}
